﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoreComplexNHibernateTest
{
    public class Order
    {
        public virtual int Id { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual Customer Customer_ { get; set; }
        public virtual  IList<Product> Products { get; set; }

        public Order()
        {
            Date = DateTime.Now;
            Products = new List<Product>();
        }
    }
}
