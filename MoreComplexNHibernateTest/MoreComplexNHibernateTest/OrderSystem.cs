﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoreComplexNHibernateTest
{
    public class OrderSystem
    {
        public List<Customer> Customers { get; set; }

        public OrderSystem()
        {
            Customers = new List<Customer>();
        }

    }
}
