﻿using NHibernate;
using NHibernate.Cfg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoreComplexNHibernateTest
{
    public partial class MainForm : Form
    {
        private ISessionFactory m_SessionFactory;
        private ISession mySession;
        public MainForm()
        {
            InitializeComponent();
            // Initialize
            Configuration cfg = new Configuration();
            cfg.Configure();

            // Create session factory from configuration object
            m_SessionFactory = cfg.BuildSessionFactory();

            
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            mySession = m_SessionFactory.OpenSession();

            using (mySession.BeginTransaction())
            {
                // Insert two employees in Database
                Customer Vasilii = new Customer()
                {
                    Name = "Vasilii",
                    Address = new Address()
                    {
                        City = "Kh",
                        State = "Kh region",
                        StreetAddress = "Klochkovskaya Str., 45",
                        Zip = "61000"
                    }
                };
            
                mySession.SaveOrUpdate(Vasilii);
                Product p1 = new Product()
                {
                    Name = "TV"
                };

                mySession.SaveOrUpdate(p1);

                Order order1 = new Order()
                {
                    Customer_ = Vasilii,
                    Products = new List<Product>()
                            {
                                p1
                            }
                };

                mySession.SaveOrUpdate(order1);

                Vasilii.Orders.Add(order1);

                mySession.SaveOrUpdate(Vasilii);
                //mySession.SaveOrUpdate(new Product() { Name = "Notebook" });
                //mySession.SaveOrUpdate(new Product() { Name = "Refegerator" });

                mySession.Transaction.Commit();

                dgv_Customers.DataSource = mySession;
            }
        }
    }
}
