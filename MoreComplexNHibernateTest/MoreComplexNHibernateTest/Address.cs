﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoreComplexNHibernateTest
{
    public class Address
    {
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string StreetAddress { get; set; }
        public virtual string Zip { get; set; }
    }
}
