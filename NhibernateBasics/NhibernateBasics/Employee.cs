﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhibernateBasics
{
    public class Employee
    {
       
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }

        public Employee()
        {
            Id = -1;
            Name = string.Empty;
        }
        public Employee(int id, string name)
        {
            Id = id;
            Name = name;
        }
        public Employee(string name)
        {
            Name = name;
        }
    }
}
