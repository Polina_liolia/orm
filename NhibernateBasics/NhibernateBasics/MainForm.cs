﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Cfg;
using System.Reflection;

namespace NhibernateBasics
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            // Initialize
            Configuration cfg = new Configuration();
            cfg.Configure();

        
            // Create session factory from configuration object
            ISessionFactory m_SessionFactory = cfg.BuildSessionFactory();
            ISession mySession = m_SessionFactory.OpenSession();

            using (mySession.BeginTransaction())
            {
                // Insert two employees in Database
                mySession.SaveOrUpdate(new Employee(1, "vasiaFirst"));
                mySession.SaveOrUpdate(new Employee(2, "petiaFirst"));
                mySession.Transaction.Commit();
            }
        }
    }
}
